package com.marakana.android.auldlangsyne;

public class CONST {

	public static class CHORD {

		public static final String C = "C-Chord";
		public static final String B = "B-Chord";
		public static final String D = "D-Chord";
		public static final String D_SHARP = "D#-Chord";
		public static final String E = "E-Chord";
		public static final String F = "F-Chord";
		public static final String G = "G-Chord";
	}

	private CONST() {
	}

	public static class TONES {

		public static final String C = "C";
		public static final String D = "D";
		public static final String D_SHARP = "D#";

	}

}
