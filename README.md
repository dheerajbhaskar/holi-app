# Holi Fun 

# What is this app for?
This app is used to create the cool effect seen on this video.
https://www.youtube.com/watch?v=BwxnXwIRJy0

# Holi and this app. Why?
This app contains musical chords (B,C,D,D#,E,F and G).The notes used to play the really popular holi song 'Rang Barse' (http://www.youtube.com/watch?v=fQX7iuvmNso)

# How do I use this app (Short version)?
Touch Top half of screen - Text is shown
Touch Bottom half of screen - Color is shown and musical chord plays

# How do I use this app (Long version)?
1) Have a video camera facing you in a dark room (no jokes here :P)
2) Load this app on multiple devices - phones, tablets etc
3) Set a musical note and color to each device
4) Follow the notes here and play along: http://justpaste.it/RangBarseNotes
5) Touch Top half of screen - Text is shown
Touch Bottom half of screen - Color is shown and musical chord plays

# I love it. Is it open source?
Yep. Find the code here - https://bitbucket.org/dheeB/holi-app

# Wait I want something more. Will you help me?
You can contact me via mail (dheeraj dot bhaskar dot developer at gmail dot com)

# Inspired by Marakana (http://marakana.com)
This project is a modified form of AuldLangSyne. Notice File contains the links to original and the current sources. 

# Legal

Please see ++NOTICE++ file in this directory for copyright, license terms, and legal disclaimers.

Copyright © 2012 Marakana Inc.
Copyright © 2013 Dheeraj Bhaskar
